class Logging {
  constructor(step, remember, lastDigit, sum) {
    this.step = step; // right to left, start = 1
    this.remember = remember; // ex: 10 + 8 > 10 => remember = 1
    this.lastDigit = lastDigit; // ex: 10 + 8 = 18 => lastDigit = 8
    this.sum = sum; // ex: 10 + 8 => sum = 18
  }
}

class MyBigNumber {
  result = ""; // result of add two 2 digit numbers
  logging = []; // Logging[]

  sum = (stn1, stn2) => {
    let number1 = stn1,
      number2 = stn2,
      remember = 0;

    const lengthStn1 = number1.length,
      lengthStn2 = number2.length;

    const zeros = this.createZerosByLength(Math.abs(lengthStn1 - lengthStn2));

    if (lengthStn1 >= lengthStn2) number2 = zeros + number2;
    else number1 = zeros + number1;

    number1 = this.reverseArrayFromStringNumber(number1); // ex: 2902 => [2,0,9,2]
    number2 = this.reverseArrayFromStringNumber(number2);

    for (const key in number1) {
      const index = parseInt(key);
      const total = number1[index] + number2[index] + remember;
      let lastDigit = 0;

      if (total < 10) {
        remember = 0;
        lastDigit = total;
      } else {
        remember = 1;
        lastDigit = total - 10;
      }

      this.result += lastDigit;
      this.logging.push(new Logging(index + 1, remember, lastDigit, total));
    }

    return this.reverseArrayFromStringNumber(this.result).join("");
  };

  createZerosByLength = (length) =>
    Array.from({
      length,
    }).reduce((total) => (total += 0), "");

  reverseArrayFromStringNumber = (str) =>
    String(str).split("").map(Number).reverse();
}

const myBigNumber = new MyBigNumber();

console.log("result of 45 + 1255: " + myBigNumber.sum("45", "1255"));
console.log(myBigNumber.logging);
